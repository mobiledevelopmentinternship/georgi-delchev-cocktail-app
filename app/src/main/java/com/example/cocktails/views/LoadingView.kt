package com.example.cocktails.views

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import androidx.constraintlayout.widget.ConstraintLayout
import com.example.cocktails.R
import kotlinx.android.synthetic.main.loading_view.view.*

class LoadingView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : ConstraintLayout(context, attrs, defStyleAttr) {

    private val loadingView: View = LayoutInflater.from(context).inflate(R.layout.loading_view, this)

    fun setProgressBarVisibility(isVisible: Boolean) {
        loadingView.progress_bar.visibility = if (isVisible) VISIBLE else GONE
    }
}