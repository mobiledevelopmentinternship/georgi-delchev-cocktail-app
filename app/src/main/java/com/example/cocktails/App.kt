package com.example.cocktails

import com.example.cocktails.dagger.component.DaggerAppComponent
import com.example.cocktails.dagger.modules.AppModule
import com.example.cocktails.dagger.modules.NetworkModule
import com.example.cocktails.dagger.modules.RoomModule
import com.example.cocktails.util.IMAGE_DOWNLOAD_HEIGHT
import com.example.cocktails.util.IMAGE_DOWNLOAD_WIDTH
import com.squareup.picasso.LruCache
import com.squareup.picasso.OkHttp3Downloader
import com.squareup.picasso.Picasso
import dagger.android.AndroidInjector
import dagger.android.support.DaggerApplication

class App : DaggerApplication() {

    companion object {

        lateinit var instance: App
    }

    override fun onCreate() {
        super.onCreate()
        instance = this

        setupPicasso()
    }

    private fun setupPicasso() {
        Picasso.setSingletonInstance(
            Picasso.Builder(this)
                .downloader(OkHttp3Downloader(this, Long.MAX_VALUE))
                .requestTransformer { request ->
                    request?.buildUpon()
                        ?.resize(IMAGE_DOWNLOAD_WIDTH, IMAGE_DOWNLOAD_HEIGHT)
                        ?.build()
                }
                .memoryCache(LruCache(this))
                .build()
        )
    }

    override fun applicationInjector(): AndroidInjector<out DaggerApplication> =
        DaggerAppComponent.builder()
            .appModule(AppModule(this))
            .networkModule(NetworkModule())
            .roomModule(RoomModule(this))
            .build()
}