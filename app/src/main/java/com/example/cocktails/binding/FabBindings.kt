package com.example.cocktails.binding

import androidx.annotation.DrawableRes
import androidx.databinding.BindingAdapter
import com.google.android.material.floatingactionbutton.FloatingActionButton

@BindingAdapter("icon")
fun setIcon(fab: FloatingActionButton, @DrawableRes drawable: Int) =
        fab.setImageResource(drawable)