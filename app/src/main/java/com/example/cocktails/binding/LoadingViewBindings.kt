package com.example.cocktails.binding

import androidx.databinding.BindingAdapter
import com.example.cocktails.views.LoadingView

@BindingAdapter("progressBarVisibility")
fun setProgressVisibility(loadingView: LoadingView, isVisible: Boolean) =
    loadingView.setProgressBarVisibility(isVisible)