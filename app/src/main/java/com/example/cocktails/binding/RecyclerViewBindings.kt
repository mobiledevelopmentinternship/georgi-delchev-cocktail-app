package com.example.cocktails.binding

import androidx.databinding.BindingAdapter
import androidx.databinding.ObservableArrayList
import androidx.lifecycle.LifecycleOwner
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.cocktails.adapters.recyclers.CocktailRecyclerAdapter
import com.example.cocktails.adapters.recyclers.base.BaseRecyclerAdapter
import com.example.cocktails.adapters.recyclers.items.base.RecyclerItem
import com.example.cocktails.ui.fragments.base.BaseVM
import com.example.cocktails.util.GRID_LAYOUT_COLUMNS
import com.example.cocktails.util.GRID_LAYOUT_DEFAULT_SPAN_COUNT

@BindingAdapter(value = ["viewModel", "items", "itemLayoutId", "lifecycleOwner"], requireAll = false)
fun <T : RecyclerItem> setupRecyclerView(
    view: RecyclerView,
    viewModel: BaseVM,
    items: ObservableArrayList<T>,
    itemLayoutId: Int,
    lifecycleOwner: LifecycleOwner?
) {
    if (view.layoutManager is GridLayoutManager &&
        (view.layoutManager as GridLayoutManager).spanCount == GRID_LAYOUT_DEFAULT_SPAN_COUNT
    ) {
        (view.layoutManager as GridLayoutManager).spanCount = GRID_LAYOUT_COLUMNS
    }

    if (view.adapter == null) {
        view.adapter = CocktailRecyclerAdapter(items, viewModel, itemLayoutId)
        if (lifecycleOwner != null) {
            (view.adapter as BaseRecyclerAdapter<T>).setLifecycleOwner(lifecycleOwner)
        }
    } else {
        (view.adapter as BaseRecyclerAdapter<T>).setItems(items)
    }
}