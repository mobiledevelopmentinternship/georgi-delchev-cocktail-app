package com.example.cocktails.binding

import android.util.Patterns
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.example.cocktails.R
import com.example.cocktails.util.PicassoImageLoader
import com.squareup.picasso.MemoryPolicy
import com.squareup.picasso.Picasso
import java.io.File

@BindingAdapter("imgViewSrc")
fun setImgViewSrc(imgView: ImageView, imageUrl: String) {
    when {
        Patterns.WEB_URL.matcher(imageUrl).matches() -> PicassoImageLoader.setImage(imgView, imageUrl)
        else -> Picasso.get().load(File(imageUrl))
            .placeholder(R.drawable.no_img)
            .memoryPolicy(MemoryPolicy.NO_CACHE)
            .into(imgView)
    }
}