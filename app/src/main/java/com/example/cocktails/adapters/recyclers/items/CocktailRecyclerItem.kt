package com.example.cocktails.adapters.recyclers.items

import android.os.Parcel
import android.os.Parcelable
import androidx.lifecycle.LifecycleObserver
import com.example.cocktails.R
import com.example.cocktails.adapters.recyclers.items.base.RecyclerItem

data class CocktailRecyclerItem(
    var id: Int,
    var name: String,
    var alcoholic: Int,
    var instructions: String?,
    var imageUrl: String?,
    var isFavorited: Boolean,
    var isCreatedByMe: Boolean
) : LifecycleObserver, RecyclerItem, Parcelable {

    override fun getItemLayoutId() = R.layout.item_collection_card

    constructor(parcel: Parcel) : this(
        parcel.readInt(),
        parcel.readString(),
        parcel.readInt(),
        parcel.readString(),
        parcel.readString(),
        parcel.readValue(Boolean::class.java.classLoader) as Boolean,
        parcel.readValue(Boolean::class.java.classLoader) as Boolean
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(name)
        parcel.writeInt(alcoholic)
        parcel.writeString(instructions)
        parcel.writeString(imageUrl)
        parcel.writeInt(id)
        parcel.writeValue(isFavorited)
        parcel.writeValue(isCreatedByMe)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<CocktailRecyclerItem> {
        override fun createFromParcel(parcel: Parcel): CocktailRecyclerItem {
            return CocktailRecyclerItem(parcel)
        }

        override fun newArray(size: Int): Array<CocktailRecyclerItem?> {
            return arrayOfNulls(size)
        }
    }
}