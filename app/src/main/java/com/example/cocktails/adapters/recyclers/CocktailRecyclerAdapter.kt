package com.example.cocktails.adapters.recyclers

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.databinding.ObservableArrayList
import androidx.databinding.ViewDataBinding
import com.example.cocktails.adapters.recyclers.base.BaseRecyclerAdapter
import com.example.cocktails.adapters.recyclers.items.base.RecyclerItem
import com.example.cocktails.ui.fragments.base.BaseVM

class CocktailRecyclerAdapter<T : RecyclerItem>(
    items: ObservableArrayList<T>,
    viewModel: BaseVM,
    @LayoutRes
    private var layoutId: Int
) : BaseRecyclerAdapter<T>(items, viewModel) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseRecyclerViewHolder<T, ViewDataBinding> {
        val inflater = LayoutInflater.from(parent.context)

        return CocktailViewHolder(DataBindingUtil.inflate(inflater, layoutId, parent, false))
    }

    constructor(
        items: ObservableArrayList<T>,
        viewModel: BaseVM
    ) : this(items, viewModel, 0)

    override fun getLayoutId(viewType: Int) = layoutId

    inner class CocktailViewHolder(binding: ViewDataBinding) :
        BaseRecyclerViewHolder<T, ViewDataBinding>(binding)
}