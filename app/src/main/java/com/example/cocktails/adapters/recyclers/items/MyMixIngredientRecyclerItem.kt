package com.example.cocktails.adapters.recyclers.items

import androidx.lifecycle.LifecycleObserver
import com.example.cocktails.R
import com.example.cocktails.adapters.recyclers.items.base.RecyclerItem

data class MyMixIngredientRecyclerItem(
    var ingredient: String? = "",
    var measure: String? = ""
) : LifecycleObserver, RecyclerItem {

    override fun getItemLayoutId(): Int = R.layout.item_my_mixture_ingredients
}