package com.example.cocktails.adapters.recyclers.items.base

interface RecyclerItem {

    fun getItemLayoutId(): Int
}