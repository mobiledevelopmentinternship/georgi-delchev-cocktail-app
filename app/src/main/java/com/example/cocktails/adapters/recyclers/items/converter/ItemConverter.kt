package com.example.cocktails.adapters.recyclers.items.converter

import com.example.cocktails.App
import com.example.cocktails.R
import com.example.cocktails.adapters.recyclers.items.CocktailRecyclerItem
import com.example.cocktails.persistance.models.CocktailDetailsPersist

object ItemConverter {

    fun convertCocktailPersistToRecyclerItem(cocktail: CocktailDetailsPersist) =
        CocktailRecyclerItem(
            cocktail.drinkId,
            cocktail.name,
            cocktail.alcoholic,
            if (cocktail.instructions.isNullOrEmpty())
                App.instance.getString(R.string.cocktail_details_generic_instructions)
            else
                cocktail.instructions,
            cocktail.imageUrl,
            cocktail.isFavorited,
            cocktail.isCreatedByMe
        )

    fun convertRecyclerItemToCocktailPersist(recyclerItem: CocktailRecyclerItem) = CocktailDetailsPersist(
        recyclerItem.name,
        recyclerItem.id,
        recyclerItem.alcoholic,
        recyclerItem.instructions,
        recyclerItem.imageUrl,
        recyclerItem.isFavorited,
        recyclerItem.isCreatedByMe
    )
}