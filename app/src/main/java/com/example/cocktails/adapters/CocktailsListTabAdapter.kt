package com.example.cocktails.adapters

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import com.example.cocktails.util.FRAGMENT_TAG_BUNDLE_KEY

class CocktailsListTabAdapter(
    fragmentManager: FragmentManager,
    val fragments: List<Fragment>
) : FragmentStatePagerAdapter(fragmentManager) {

    override fun getItem(position: Int): Fragment = fragments[position]

    override fun getCount(): Int = fragments.size

    override fun getPageTitle(position: Int): CharSequence? =
        fragments[position].arguments?.get(FRAGMENT_TAG_BUNDLE_KEY) as CharSequence?
}