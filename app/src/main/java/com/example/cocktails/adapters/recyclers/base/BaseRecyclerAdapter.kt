package com.example.cocktails.adapters.recyclers.base

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.databinding.ObservableArrayList
import androidx.databinding.ObservableList
import androidx.databinding.ViewDataBinding
import androidx.databinding.library.baseAdapters.BR
import androidx.lifecycle.LifecycleOwner
import androidx.recyclerview.widget.RecyclerView
import com.example.cocktails.adapters.recyclers.items.base.RecyclerItem
import com.example.cocktails.ui.fragments.base.BaseVM

abstract class BaseRecyclerAdapter<T : RecyclerItem>(
    private var items: ObservableArrayList<T>,
    private var viewModel: BaseVM
) : RecyclerView.Adapter<BaseRecyclerAdapter<T>.BaseRecyclerViewHolder<T, ViewDataBinding>>() {

    private var lifecycleOwner: LifecycleOwner? = null

    init {
        initOnListChangeListener()
    }

    @LayoutRes
    protected abstract fun getLayoutId(viewType: Int): Int

    abstract override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): BaseRecyclerViewHolder<T, ViewDataBinding>

    override fun onBindViewHolder(holder: BaseRecyclerViewHolder<T, ViewDataBinding>, position: Int) {
        val item: T = this.items[position]
        holder.bind(item)
    }

    override fun getItemCount(): Int = if (items.size > 0) items.size else 0

    open fun getItem(position: Int): T = this.items[position]

    private fun getViewHolderBinding(parent: ViewGroup, @LayoutRes itemLayoutId: Int): ViewDataBinding =
        DataBindingUtil.inflate(LayoutInflater.from(parent.context), itemLayoutId, parent, false)

    fun getLifecycleOwner() = lifecycleOwner

    open fun setLifecycleOwner(lifecycleOwner: LifecycleOwner) {
        this.lifecycleOwner = lifecycleOwner
    }

    open fun setItems(items: ObservableArrayList<T>) {
        this.items = items
    }

    private fun initOnListChangeListener() {
        val listener = object : ObservableList.OnListChangedCallback<ObservableArrayList<T>>() {
            override fun onChanged(sender: ObservableArrayList<T>?) {
                notifyDataSetChanged()
            }

            override fun onItemRangeRemoved(sender: ObservableArrayList<T>?, positionStart: Int, itemCount: Int) {
                notifyItemRangeRemoved(positionStart, itemCount)
            }

            override fun onItemRangeMoved(
                sender: ObservableArrayList<T>?,
                fromPosition: Int,
                toPosition: Int,
                itemCount: Int
            ) {
                notifyDataSetChanged()
            }

            override fun onItemRangeInserted(sender: ObservableArrayList<T>?, positionStart: Int, itemCount: Int) {
                notifyItemRangeInserted(positionStart, itemCount)
            }

            override fun onItemRangeChanged(sender: ObservableArrayList<T>?, positionStart: Int, itemCount: Int) {
                notifyItemRangeChanged(positionStart, itemCount)
            }
        }

        items.addOnListChangedCallback(listener)
    }

    abstract inner class BaseRecyclerViewHolder<T, B : ViewDataBinding>(
        private var binder: B
    ) : RecyclerView.ViewHolder(binder.root) {

        open fun bind(item: T) {
            binder.setVariable(BR.viewModel, viewModel)
            binder.setVariable(BR.item, item)
            binder.executePendingBindings()
            if (lifecycleOwner != null) {
                binder.lifecycleOwner = lifecycleOwner
            }
        }
    }
}