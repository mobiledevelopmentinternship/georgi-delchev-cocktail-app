package com.example.cocktails.persistance.models

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class CocktailDetailsPersist(
    @PrimaryKey
    @ColumnInfo(name = "name")
    val name: String,
    @ColumnInfo(name = "id")
    val drinkId: Int,
    @ColumnInfo(name = "type")
    val alcoholic: Int,
    @ColumnInfo(name = "instructions")
    var instructions: String?,
    @ColumnInfo(name = "image_url")
    val imageUrl: String?,
    @ColumnInfo(name = "is_favorited")
    var isFavorited: Boolean = false,
    @ColumnInfo(name = "is_created_by_me")
    var isCreatedByMe: Boolean = false
)