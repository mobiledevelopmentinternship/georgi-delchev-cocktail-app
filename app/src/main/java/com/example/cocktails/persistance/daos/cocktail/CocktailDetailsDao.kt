package com.example.cocktails.persistance.daos.cocktail

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Query
import com.example.cocktails.persistance.daos.base.BaseDao
import com.example.cocktails.persistance.models.CocktailDetailsPersist
import com.example.cocktails.persistance.models.IngredientPersist
import com.example.cocktails.util.ALCOHOLIC_DRINK_ID
import com.example.cocktails.util.NON_ALCOHOLIC_DRINK_ID
import io.reactivex.Single

@Dao
interface CocktailDetailsDao : BaseDao<CocktailDetailsPersist> {

    @Query("SELECT * FROM CocktailDetailsPersist as cd WHERE cd.id = :id")
    fun getCocktailsById(id: Int): Single<CocktailDetailsPersist>

    @Query("SELECT * FROM IngredientPersist as ingr WHERE ingr.cocktail_name = :cocktailName")
    fun getCocktailIngredients(cocktailName: String): Single<List<IngredientPersist>>

    @Query("SELECT * FROM CocktailDetailsPersist as cd WHERE cd.type = $ALCOHOLIC_DRINK_ID")
    fun getAllAlcoholicCocktails(): Single<List<CocktailDetailsPersist>>

    @Query("SELECT * FROM CocktailDetailsPersist as cd WHERE cd.type = $NON_ALCOHOLIC_DRINK_ID")
    fun getAllNonAlcoholicCocktails(): Single<List<CocktailDetailsPersist>>

    @Query("SELECT * FROM CocktailDetailsPersist as cd WHERE cd.is_favorited = 1")
    fun getFavoriteCocktails(): LiveData<List<CocktailDetailsPersist>>

    @Query("SELECT * FROM CocktailDetailsPersist as cd WHERE cd.is_favorited = 1")
    fun getAllFavoriteCocktails(): Single<List<CocktailDetailsPersist>>

    @Query("SELECT * FROM CocktailDetailsPersist as cd WHERE cd.is_created_by_me = 1")
    fun getCreatedCocktails(): LiveData<List<CocktailDetailsPersist>>
}