package com.example.cocktails.persistance.daos.ingredients

import androidx.room.Dao
import com.example.cocktails.persistance.daos.base.BaseDao
import com.example.cocktails.persistance.models.IngredientPersist

@Dao
interface IngredientsDao : BaseDao<IngredientPersist>