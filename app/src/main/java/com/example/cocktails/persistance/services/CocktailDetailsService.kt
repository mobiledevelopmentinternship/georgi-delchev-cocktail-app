package com.example.cocktails.persistance.services

import androidx.lifecycle.LiveData
import com.example.cocktails.persistance.daos.cocktail.CocktailDetailsDao
import com.example.cocktails.persistance.executors.AppExecutor
import com.example.cocktails.persistance.models.CocktailDetailsPersist
import com.example.cocktails.persistance.models.IngredientPersist
import com.example.cocktails.persistance.repositories.cocktails.CocktailDetailsRepository
import io.reactivex.Single
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class CocktailDetailsService @Inject constructor(
    private val cocktailDetailsDao: CocktailDetailsDao,
    private val executor: AppExecutor
) : CocktailDetailsRepository {
    override fun getAllFavoriteCocktails(): Single<List<CocktailDetailsPersist>> =
        cocktailDetailsDao.getAllFavoriteCocktails()

    override fun getCreatedCocktails(): LiveData<List<CocktailDetailsPersist>> =
        cocktailDetailsDao.getCreatedCocktails()

    override fun getFavoriteCocktails(): LiveData<List<CocktailDetailsPersist>> =
        cocktailDetailsDao.getFavoriteCocktails()

    override fun getAllAlcoholicCocktails(): Single<List<CocktailDetailsPersist>> =
        cocktailDetailsDao.getAllAlcoholicCocktails()

    override fun getAllNonAlcoholicCocktails(): Single<List<CocktailDetailsPersist>> =
        cocktailDetailsDao.getAllNonAlcoholicCocktails()

    override fun getCocktailById(id: Int): Single<CocktailDetailsPersist> = cocktailDetailsDao.getCocktailsById(id)

    override fun getCocktailIngredients(cocktailName: String): Single<List<IngredientPersist>> =
        cocktailDetailsDao.getCocktailIngredients(cocktailName)

    override fun insert(data: CocktailDetailsPersist) = executor.execute { cocktailDetailsDao.insert(data) }

    override fun insertAll(data: List<CocktailDetailsPersist>) = executor.execute { cocktailDetailsDao.insertAll(data) }

    override fun update(data: CocktailDetailsPersist) = executor.execute { cocktailDetailsDao.update(data) }

    override fun delete(data: CocktailDetailsPersist) = executor.execute { cocktailDetailsDao.delete(data) }
}