package com.example.cocktails.persistance.daos.base

import androidx.room.*

@Dao
interface BaseDao<T> {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(data: T)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(data: List<T>)

    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun update(data: T)

    @Delete
    fun delete(data: T)
}