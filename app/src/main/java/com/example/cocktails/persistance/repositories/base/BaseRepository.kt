package com.example.cocktails.persistance.repositories.base

interface BaseRepository<T> {

    fun insert(data: T)

    fun insertAll(data: List<T>)

    fun update(data: T)

    fun delete(data: T)
}