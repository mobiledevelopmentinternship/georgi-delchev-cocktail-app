package com.example.cocktails.persistance.executors

import java.util.concurrent.Executor

class AppExecutor(private val executor: Executor) {

    fun execute(runnable: () -> Unit) {
        executor.execute(runnable)
    }
}