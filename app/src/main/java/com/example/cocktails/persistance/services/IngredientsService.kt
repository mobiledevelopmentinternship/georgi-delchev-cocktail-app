package com.example.cocktails.persistance.services

import com.example.cocktails.persistance.daos.ingredients.IngredientsDao
import com.example.cocktails.persistance.executors.AppExecutor
import com.example.cocktails.persistance.models.IngredientPersist
import com.example.cocktails.persistance.repositories.ingredients.IngredientRepository
import javax.inject.Inject

class IngredientsService @Inject constructor(
    private val dao: IngredientsDao,
    private val executor: AppExecutor
) : IngredientRepository {

    override fun insert(data: IngredientPersist) = executor.execute { dao.insert(data) }

    override fun insertAll(data: List<IngredientPersist>) = executor.execute { dao.insertAll(data) }

    override fun update(data: IngredientPersist) = executor.execute { dao.update(data) }

    override fun delete(data: IngredientPersist) = executor.execute { dao.delete(data) }
}