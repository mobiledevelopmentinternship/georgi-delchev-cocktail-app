package com.example.cocktails.persistance.repositories.ingredients

import com.example.cocktails.persistance.models.IngredientPersist
import com.example.cocktails.persistance.repositories.base.BaseRepository

interface IngredientRepository : BaseRepository<IngredientPersist>