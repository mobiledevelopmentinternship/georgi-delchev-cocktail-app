package com.example.cocktails.persistance.models

import android.os.Parcel
import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class IngredientPersist(
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    val ingredientId: Int?,
    @ColumnInfo(name = "ingredient_name")
    val name: String,
    @ColumnInfo(name = "amount")
    val amount: String,
    @ColumnInfo(name = "cocktail_name")
    val cocktailName: String
) : Parcelable {

    constructor(parcel: Parcel) : this(
        parcel.readInt(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString()
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(ingredientId!!)
        parcel.writeString(name)
        parcel.writeString(amount)
        parcel.writeString(cocktailName)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<IngredientPersist> {
        override fun createFromParcel(parcel: Parcel): IngredientPersist {
            return IngredientPersist(parcel)
        }

        override fun newArray(size: Int): Array<IngredientPersist?> {
            return arrayOfNulls(size)
        }
    }
}