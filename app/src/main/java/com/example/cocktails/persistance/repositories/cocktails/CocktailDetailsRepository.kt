package com.example.cocktails.persistance.repositories.cocktails

import androidx.lifecycle.LiveData
import com.example.cocktails.persistance.models.CocktailDetailsPersist
import com.example.cocktails.persistance.models.IngredientPersist
import com.example.cocktails.persistance.repositories.base.BaseRepository
import io.reactivex.Single

interface CocktailDetailsRepository : BaseRepository<CocktailDetailsPersist> {

    fun getCocktailById(id: Int): Single<CocktailDetailsPersist>

    fun getCocktailIngredients(cocktailName: String): Single<List<IngredientPersist>>

    fun getAllAlcoholicCocktails(): Single<List<CocktailDetailsPersist>>

    fun getAllNonAlcoholicCocktails(): Single<List<CocktailDetailsPersist>>

    fun getFavoriteCocktails(): LiveData<List<CocktailDetailsPersist>>

    fun getAllFavoriteCocktails(): Single<List<CocktailDetailsPersist>>

    fun getCreatedCocktails(): LiveData<List<CocktailDetailsPersist>>
}