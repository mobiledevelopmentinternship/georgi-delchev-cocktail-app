package com.example.cocktails.persistance.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.cocktails.persistance.daos.cocktail.CocktailDetailsDao
import com.example.cocktails.persistance.daos.ingredients.IngredientsDao
import com.example.cocktails.persistance.models.CocktailDetailsPersist
import com.example.cocktails.persistance.models.IngredientPersist

@Database(
    entities = [
        IngredientPersist::class,
        CocktailDetailsPersist::class
    ],
    version = 1,
    exportSchema = false
)
abstract class CocktailDb : RoomDatabase() {

    abstract fun cocktailDetailsDao(): CocktailDetailsDao

    abstract fun ingredientDao(): IngredientsDao
}