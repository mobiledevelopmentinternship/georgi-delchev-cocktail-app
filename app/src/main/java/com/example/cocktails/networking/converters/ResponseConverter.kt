package com.example.cocktails.networking.converters

import com.example.cocktails.networking.response.CocktailResponse
import com.example.cocktails.networking.response.DrinkResponse
import com.example.cocktails.persistance.models.CocktailDetailsPersist
import com.example.cocktails.persistance.models.IngredientPersist
import com.example.cocktails.util.ALCOHOLIC
import com.example.cocktails.util.ALCOHOLIC_DRINK_ID
import com.example.cocktails.util.NON_ALCOHOLIC_DRINK_ID

object ResponseConverter {

    fun convertCocktailDetailsResponse(allDrinks: DrinkResponse): MutableList<CocktailDetailsPersist> {
        val cocktails: MutableList<CocktailDetailsPersist> = mutableListOf()
        for (cocktail in allDrinks.drinks) {
            cocktails.add(
                CocktailDetailsPersist(
                    cocktail.cocktailName,
                    cocktail.cocktailId,
                    if (cocktail.alcoholic == ALCOHOLIC) ALCOHOLIC_DRINK_ID else NON_ALCOHOLIC_DRINK_ID,
                    cocktail.instructions,
                    cocktail.imageUrl
                )
            )
        }

        return cocktails
    }

    fun convertAlcoholicCocktailsResponse(allDrinks: DrinkResponse): MutableList<CocktailDetailsPersist> {
        val cocktails: MutableList<CocktailDetailsPersist> = mutableListOf()
        for (cocktail in allDrinks.drinks) {
            cocktails.add(
                CocktailDetailsPersist(
                    cocktail.cocktailName,
                    cocktail.cocktailId,
                    ALCOHOLIC_DRINK_ID,
                    cocktail.instructions,
                    cocktail.imageUrl
                )
            )
        }

        return cocktails
    }

    fun convertNonAlcoholicCocktailsResponse(allDrinks: DrinkResponse): MutableList<CocktailDetailsPersist> {
        val cocktails: MutableList<CocktailDetailsPersist> = mutableListOf()
        for (cocktail in allDrinks.drinks) {
            cocktails.add(
                CocktailDetailsPersist(
                    cocktail.cocktailName,
                    cocktail.cocktailId,
                    NON_ALCOHOLIC_DRINK_ID,
                    cocktail.instructions,
                    cocktail.imageUrl
                )
            )
        }

        return cocktails
    }

    fun extractCocktailIngredientsAndMeasuresFromResponse(cocktail: CocktailResponse): List<IngredientPersist> {
        return cocktail.getIngredientsList().mapIndexed { index, ingredient ->
            IngredientPersist(
                null,
                ingredient,
                if (index >= cocktail.getMeasuresList().size) ""
                else cocktail.getMeasuresList()[index],
                cocktail.cocktailName
            )
        }
    }
}
