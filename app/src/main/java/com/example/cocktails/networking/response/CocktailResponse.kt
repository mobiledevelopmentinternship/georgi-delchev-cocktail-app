package com.example.cocktails.networking.response

import com.google.gson.annotations.SerializedName

data class CocktailResponse(
    @SerializedName("idDrink")
    val cocktailId: Int,
    @SerializedName("strDrink")
    val cocktailName: String,
    @SerializedName("strAlcoholic")
    val alcoholic: String,
    @SerializedName("strInstructions")
    val instructions: String,
    @SerializedName("strDrinkThumb")
    val imageUrl: String,
    @SerializedName("strIngredient1")
    val ingredient1: String = "",
    @SerializedName("strIngredient2")
    val ingredient2: String = "",
    @SerializedName("strIngredient3")
    val ingredient3: String = "",
    @SerializedName("strIngredient4")
    val ingredient4: String = "",
    @SerializedName("strIngredient5")
    val ingredient5: String = "",
    @SerializedName("strIngredient6")
    val ingredient6: String = "",
    @SerializedName("strIngredient7")
    val ingredient7: String = "",
    @SerializedName("strIngredient8")
    val ingredient8: String = "",
    @SerializedName("strIngredient9")
    val ingredient9: String = "",
    @SerializedName("strIngredient10")
    val ingredient10: String = "",
    @SerializedName("strIngredient11")
    val ingredient11: String = "",
    @SerializedName("strIngredient12")
    val ingredient12: String = "",
    @SerializedName("strIngredient13")
    val ingredient13: String = "",
    @SerializedName("strIngredient14")
    val ingredient14: String = "",
    @SerializedName("strIngredient15")
    val ingredient15: String = "",
    @SerializedName("strMeasure1")
    val measure1: String = "",
    @SerializedName("strMeasure2")
    val measure2: String = "",
    @SerializedName("strMeasure3")
    val measure3: String = "",
    @SerializedName("strMeasure4")
    val measure4: String = "",
    @SerializedName("strMeasure5")
    val measure5: String = "",
    @SerializedName("strMeasure6")
    val measure6: String = "",
    @SerializedName("strMeasure7")
    val measure7: String = "",
    @SerializedName("strMeasure8")
    val measure8: String = "",
    @SerializedName("strMeasure9")
    val measure9: String = "",
    @SerializedName("strMeasure10")
    val measure10: String = "",
    @SerializedName("strMeasure11")
    val measure11: String = "",
    @SerializedName("strMeasure12")
    val measure12: String = "",
    @SerializedName("strMeasure13")
    val measure13: String = "",
    @SerializedName("strMeasure14")
    val measure14: String = "",
    @SerializedName("strMeasure15")
    val measure15: String = ""
) {

    fun getIngredientsList(): List<String> {
        val ingredients : MutableList<String> = mutableListOf()
        if (ingredient1.isNotEmpty()) ingredients.add(this.ingredient1)
        if (ingredient2.isNotEmpty()) ingredients.add(this.ingredient2)
        if (ingredient3.isNotEmpty()) ingredients.add(this.ingredient3)
        if (ingredient4.isNotEmpty()) ingredients.add(this.ingredient4)
        if (ingredient5.isNotEmpty()) ingredients.add(this.ingredient5)
        if (ingredient6.isNotEmpty()) ingredients.add(this.ingredient6)
        if (ingredient7.isNotEmpty()) ingredients.add(this.ingredient7)
        if (ingredient8.isNotEmpty()) ingredients.add(this.ingredient8)
        if (ingredient9.isNotEmpty()) ingredients.add(this.ingredient9)
        if (ingredient10.isNotEmpty()) ingredients.add(this.ingredient10)
        if (ingredient11.isNotEmpty()) ingredients.add(this.ingredient11)
        if (ingredient12.isNotEmpty()) ingredients.add(this.ingredient12)
        if (ingredient13.isNotEmpty()) ingredients.add(this.ingredient13)
        if (ingredient14.isNotEmpty()) ingredients.add(this.ingredient14)
        if (ingredient15.isNotEmpty()) ingredients.add(this.ingredient15)

        return ingredients
    }

    fun getMeasuresList(): List<String> {
        val measures : MutableList<String> = mutableListOf()
        if (measure1.isNotEmpty()) measures.add(this.measure1)
        if (measure2.isNotEmpty()) measures.add(this.measure2)
        if (measure3.isNotEmpty()) measures.add(this.measure3)
        if (measure4.isNotEmpty()) measures.add(this.measure4)
        if (measure5.isNotEmpty()) measures.add(this.measure5)
        if (measure6.isNotEmpty()) measures.add(this.measure6)
        if (measure7.isNotEmpty()) measures.add(this.measure7)
        if (measure8.isNotEmpty()) measures.add(this.measure8)
        if (measure9.isNotEmpty()) measures.add(this.measure9)
        if (measure10.isNotEmpty()) measures.add(this.measure10)
        if (measure11.isNotEmpty()) measures.add(this.measure11)
        if (measure12.isNotEmpty()) measures.add(this.measure12)
        if (measure13.isNotEmpty()) measures.add(this.measure13)
        if (measure14.isNotEmpty()) measures.add(this.measure14)
        if (measure15.isNotEmpty()) measures.add(this.measure15)

        return measures
    }
}