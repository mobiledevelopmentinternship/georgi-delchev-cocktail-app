package com.example.cocktails.networking.response

import com.google.gson.annotations.SerializedName

data class DrinkResponse(
    @SerializedName("drinks")
    val drinks: List<CocktailResponse>
)