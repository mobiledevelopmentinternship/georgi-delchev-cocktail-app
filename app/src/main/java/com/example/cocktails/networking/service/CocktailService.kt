package com.example.cocktails.networking.service

import com.example.cocktails.networking.api.CocktailApi
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class CocktailService
@Inject constructor(private val cocktailApi: CocktailApi) {

    fun getRandomCocktail() = cocktailApi.getRandomCocktail()

    fun getAlcoholicCocktails() = cocktailApi.getAlcoholicCocktails()

    fun getNonAlcoholicCocktails() = cocktailApi.getNonAlcoholicCocktails()

    fun getFullCocktailDetails(id: Int) = cocktailApi.getFullCocktailDetails(id)
}