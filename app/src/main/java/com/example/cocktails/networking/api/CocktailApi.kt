package com.example.cocktails.networking.api

import com.example.cocktails.networking.response.DrinkResponse
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface CocktailApi {

    @GET("random.php")
    fun getRandomCocktail(): Single<DrinkResponse>

    @GET("filter.php?a=Alcoholic")
    fun getAlcoholicCocktails(): Single<DrinkResponse>

    @GET("filter.php?a=Non_Alcoholic")
    fun getNonAlcoholicCocktails(): Single<DrinkResponse>

    @GET("lookup.php")
    fun getFullCocktailDetails(@Query("i") id: Int): Single<DrinkResponse>
}