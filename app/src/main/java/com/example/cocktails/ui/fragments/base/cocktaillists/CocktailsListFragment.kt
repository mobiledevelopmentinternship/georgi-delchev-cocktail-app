package com.example.cocktails.ui.fragments.base.cocktaillists

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.navArgs
import com.example.cocktails.R
import com.example.cocktails.R.string.favorites_tab_name
import com.example.cocktails.R.string.my_mixes_tab_name
import com.example.cocktails.adapters.CocktailsListTabAdapter
import com.example.cocktails.ui.fragments.favorites.favorites.FavoriteCocktailsFragment
import com.example.cocktails.ui.fragments.favorites.mymixes.MyCocktailMixesFragment
import com.example.cocktails.ui.fragments.menu.alcoholic.AlcoholicCocktailsCollectionFragment
import com.example.cocktails.ui.fragments.menu.nonalcoholic.NonAlcoholicCocktailsCollectionFragment
import kotlinx.android.synthetic.main.fragment_cocktails_list.*

class CocktailsListFragment : Fragment() {

    private val arguments by navArgs<CocktailsListFragmentArgs>()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View =
        inflater.inflate(R.layout.fragment_cocktails_list, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val loadFavorites: Boolean = arguments.loadFavorites

        loadTabs(loadFavorites)
    }

    private fun loadTabs(loadFavorites: Boolean) {
        if (loadFavorites) {
            val fragments = mutableListOf(
                FavoriteCocktailsFragment.newInstance(getString(favorites_tab_name)),
                MyCocktailMixesFragment.newInstance(getString(my_mixes_tab_name))
            )

            view_pager_cocktails_list.adapter = CocktailsListTabAdapter(childFragmentManager, fragments)
            tab_layout_cocktail_collection.setupWithViewPager(view_pager_cocktails_list)
        } else {
            val fragments = listOf(
                AlcoholicCocktailsCollectionFragment.newInstance(getString(R.string.cocktail_menu_alcoholic_tab_name)),
                NonAlcoholicCocktailsCollectionFragment.newInstance(getString(R.string.cocktail_menu_non_alcoholic_tab_name))
            )

            view_pager_cocktails_list.adapter = CocktailsListTabAdapter(childFragmentManager, fragments)
            tab_layout_cocktail_collection.setupWithViewPager(view_pager_cocktails_list)
        }
    }
}
