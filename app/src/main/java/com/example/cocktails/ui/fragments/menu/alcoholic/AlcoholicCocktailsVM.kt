package com.example.cocktails.ui.fragments.menu.alcoholic

import com.example.cocktails.R
import com.example.cocktails.adapters.recyclers.items.converter.ItemConverter.convertCocktailPersistToRecyclerItem
import com.example.cocktails.extensions.Event
import com.example.cocktails.persistance.models.CocktailDetailsPersist
import com.example.cocktails.ui.fragments.base.BaseCocktailsVM
import com.example.cocktails.util.ALCOHOLIC_COCKTAILS_FROM_API_COUNT
import com.example.cocktails.util.NetworkUtils
import io.reactivex.functions.Consumer
import javax.inject.Inject

class AlcoholicCocktailsVM @Inject constructor() : BaseCocktailsVM() {

    override fun onCreate() {
        getAlcoholicDrinksFromDb()
    }

    private fun getAlcoholicDrinksFromDb() {
        progressBarVisibility.value = true
        subscribeSingle(
            cocktailDetailsPersistService.getAllAlcoholicCocktails(),
            Consumer { result ->
                if (result.size < ALCOHOLIC_COCKTAILS_FROM_API_COUNT) {
                    getAlcoholicCocktailsFromApi()
                } else{
                    onQuerySuccess(result)
                }
            },
            Consumer { onQueryError() }
        )
    }

    private fun onQueryError() {
        if (NetworkUtils.isNetworkAvailable() != null) {
            getAlcoholicCocktailsFromApi()
        } else {
            error.value = Event(R.string.alert_dialog_random_cocktail_error_message)
            progressBarVisibility.value = false
        }
    }

    private fun onQuerySuccess(result: List<CocktailDetailsPersist>) {
        if (result.isNotEmpty()) {
            processResults(result)
            progressBarVisibility.value = false
        } else {
            getAlcoholicCocktailsFromApi()
        }
    }


}