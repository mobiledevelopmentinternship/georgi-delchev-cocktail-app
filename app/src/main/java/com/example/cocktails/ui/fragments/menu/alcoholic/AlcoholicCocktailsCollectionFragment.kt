package com.example.cocktails.ui.fragments.menu.alcoholic

import android.os.Bundle
import com.example.cocktails.R
import com.example.cocktails.databinding.FragmentCocktailsListBinding
import com.example.cocktails.ui.fragments.base.BaseCocktailCollectionFragment
import com.example.cocktails.util.FRAGMENT_TAG_BUNDLE_KEY

class AlcoholicCocktailsCollectionFragment :
    BaseCocktailCollectionFragment<AlcoholicCocktailsVM, FragmentCocktailsListBinding>() {

    override val viewModelClass = AlcoholicCocktailsVM::class

    override fun getLayoutId(): Int = R.layout.fragment_cocktail_collection

    companion object {
        fun newInstance(tag: String): AlcoholicCocktailsCollectionFragment {
            val alcoholicCocktailsCollectionFragment = AlcoholicCocktailsCollectionFragment()
            val bundle = Bundle()
            bundle.putString(FRAGMENT_TAG_BUNDLE_KEY, tag)
            alcoholicCocktailsCollectionFragment.arguments = bundle

            return alcoholicCocktailsCollectionFragment
        }
    }
}