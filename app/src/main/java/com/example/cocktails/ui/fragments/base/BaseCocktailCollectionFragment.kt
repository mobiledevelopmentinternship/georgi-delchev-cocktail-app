package com.example.cocktails.ui.fragments.base

import android.annotation.SuppressLint
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.text.TextUtils
import android.view.View
import android.widget.Toast
import androidx.appcompat.view.menu.MenuBuilder
import androidx.appcompat.view.menu.MenuPopupHelper
import androidx.appcompat.widget.PopupMenu
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.Observer
import com.example.cocktails.R
import com.example.cocktails.adapters.recyclers.items.CocktailRecyclerItem
import com.example.cocktails.adapters.recyclers.items.converter.ItemConverter.convertRecyclerItemToCocktailPersist
import com.example.cocktails.persistance.models.CocktailDetailsPersist

abstract class BaseCocktailCollectionFragment<V : BaseCocktailsVM, B : ViewDataBinding> :
    BaseFragment<V, ViewDataBinding>() {

    private var cocktailCardMenu: PopupMenu? = null
    private var cocktailName: String? = null
    private var lastMenuAnchor: View? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setActionBarTitle(getString(R.string.collection_fragment_title))

        observeMenuClicks()
        observeAddedToFavorites()
        observeDeletesFromFavorites()
        observeDeletesFromMyMixes()
    }

    private fun observeDeletesFromMyMixes() = viewModel.isDeletedFromMyMixes?.observe(viewLifecycleOwner,
        Observer { isDeleted ->
            if (isDeleted) {
                Toast.makeText(
                    requireContext(), TextUtils.concat(
                        getString(R.string.toast_deleted_from_my_mixes), " ", cocktailName
                    ), Toast.LENGTH_SHORT
                ).show()
            }
        })

    private fun observeDeletesFromFavorites() = viewModel.isRemovedFromFavorites?.observe(viewLifecycleOwner,
        Observer { isRemoved ->
            if (isRemoved) {
                Toast.makeText(
                    requireContext(), TextUtils.concat(
                        getString(R.string.toast_cocktail_removed), " ",
                        cocktailName, " ", getString(R.string.toast_cocktail_removed_from_favorites)
                    ),
                    Toast.LENGTH_SHORT
                ).show()
            } else {
                Toast.makeText(requireContext(), getString(R.string.toast_cant_be_deleted), Toast.LENGTH_SHORT)
                    .show()
            }
        })

    private fun observeAddedToFavorites() = viewModel.isAddedToFavorites?.observe(viewLifecycleOwner,
        Observer {
            if (it) {
                Toast.makeText(
                    requireContext(), cocktailName + " " + getString(R.string.toast_already_added), Toast.LENGTH_SHORT
                ).show()
            } else {
                Toast.makeText(
                    requireContext(), getString(R.string.toast_congrats_message) + " "
                            + cocktailName + " " + getString(R.string.toast_to_favorites), Toast.LENGTH_SHORT
                ).show()
            }
        })

    @SuppressLint("RestrictedApi")
    fun observeMenuClicks() = viewModel.menuOptionsClicked.observe(viewLifecycleOwner,
        Observer { pair ->
            if (lastMenuAnchor != pair.first) {
                lastMenuAnchor = pair.first
                cocktailCardMenu = PopupMenu(requireContext(), lastMenuAnchor!!)
                if (pair.second.isFavorited || pair.second.isCreatedByMe) {
                    cocktailCardMenu?.menuInflater?.inflate(R.menu.cocktail_card_favorites_menu, cocktailCardMenu?.menu)
                } else {
                    cocktailCardMenu?.menuInflater?.inflate(R.menu.cocktail_card_menu, cocktailCardMenu?.menu)
                }
                val menuPopupHelper = MenuPopupHelper(requireContext(), cocktailCardMenu?.menu as MenuBuilder)
                menuPopupHelper.setForceShowIcon(true)
                menuPopupHelper.setAnchorView(lastMenuAnchor!!)
                menuPopupHelper.setOnDismissListener {
                    menuPopupHelper.dismiss()
                    cocktailCardMenu = null
                    lastMenuAnchor = null
                }
                menuPopupHelper.show()

                val cocktail = convertRecyclerItemToCocktailPersist(pair.second)
                cocktailCardMenu?.setOnMenuItemClickListener(PopupMenu.OnMenuItemClickListener { item ->
                    when (item.itemId) {
                        R.id.menu_item_share -> {
                            sendCocktail(cocktail)
                            return@OnMenuItemClickListener true
                        }
                        R.id.menu_item_favorites -> {
                            cocktailName = cocktail.name
                            viewModel.saveToFavorites(cocktail)
                            return@OnMenuItemClickListener true
                        }
                        R.id.menu_item_delete -> {
                            cocktailName = cocktail.name
                            if (pair.second.isCreatedByMe) {
                                viewModel.deleteCreatedCocktail(pair.second)
                            } else {
                                viewModel.removeFromFavorites(cocktail)
                            }
                            return@OnMenuItemClickListener true
                        }
                    }
                    return@OnMenuItemClickListener true
                })
            }
        })

    private fun sendCocktail(cocktail: CocktailDetailsPersist) {
        val intent: Intent = Intent().apply {
            action = Intent.ACTION_SEND
            type = "image/*"
            this.putExtra(Intent.EXTRA_STREAM, Uri.parse(cocktail.imageUrl))
            this.putExtra(Intent.EXTRA_TEXT, cocktail.name)
            addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
        }
        Intent.createChooser(intent, getString(R.string.choose_sender))
        startActivity(intent)
    }

    protected fun updateCocktailsListContent(convertedItems: MutableList<CocktailRecyclerItem>) {
        when {
            viewModel.cocktailsList.isEmpty() -> viewModel.cocktailsList.addAll(convertedItems)
            convertedItems.size > viewModel.cocktailsList.size -> convertedItems.forEach { cocktail ->
                if (!viewModel.cocktailsList.contains(cocktail)) {
                    viewModel.cocktailsList.add(cocktail)
                    return@forEach
                }
            }
            else -> {
                var toBeRemoved: CocktailRecyclerItem? = null
                viewModel.cocktailsList.forEach { cocktail ->
                    if (!convertedItems.contains(cocktail)) {
                        toBeRemoved = cocktail
                        return@forEach
                    }
                }

                if (toBeRemoved != null) {
                    viewModel.cocktailsList.remove(toBeRemoved)
                }
            }
        }
    }

    override fun onPause() {
        viewModel.isAddedToFavorites = null
        viewModel.isRemovedFromFavorites = null
        viewModel.isDeletedFromMyMixes = null
        lifecycle.removeObserver(viewModel)
        super.onPause()
    }
}