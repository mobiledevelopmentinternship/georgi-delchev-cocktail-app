package com.example.cocktails.ui.fragments.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import com.example.cocktails.BR
import com.example.cocktails.extensions.showErrorDialog
import dagger.android.support.AndroidSupportInjection
import javax.inject.Inject
import kotlin.reflect.KClass

abstract class BaseFragment<V : BaseVM, B : ViewDataBinding> : Fragment() {

    @Inject
    lateinit var factory: ViewModelProvider.Factory

    protected lateinit var viewModel: V
    protected lateinit var binding: B
    protected abstract val viewModelClass: KClass<V>

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidSupportInjection.inject(this)
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProviders.of(this, factory).get(viewModelClass.java)
        lifecycle.addObserver(viewModel)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, getLayoutId(), container, false)
        binding.lifecycleOwner = viewLifecycleOwner
        binding.setVariable(BR.viewModel, viewModel)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observeErrors()
        observeNavigationEvents()
    }

    private fun observeNavigationEvents() =
        viewModel.newDestinationEvent.observe(viewLifecycleOwner, Observer { event ->
            if (event != null) {
                event.getContent()?.let { direction ->
                    findNavController().navigate(direction)

                }
            }
        })

    private fun observeErrors() = viewModel.error.observe(viewLifecycleOwner, Observer { event ->
        if (event != null) {
            event.getContent()?.let { error ->
                showErrorDialog(error)
            }
        }
    })

    @LayoutRes
    abstract fun getLayoutId(): Int

    protected fun setActionBarTitle(title: String): Unit? {
        return (activity as AppCompatActivity).supportActionBar?.setTitle(title)
    }

    override fun onDestroy() {
        lifecycle.removeObserver(viewModel)
        super.onDestroy()
    }
}
