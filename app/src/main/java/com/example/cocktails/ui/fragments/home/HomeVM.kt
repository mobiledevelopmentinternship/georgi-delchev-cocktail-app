package com.example.cocktails.ui.fragments.home

import android.text.TextUtils
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.OnLifecycleEvent
import com.example.cocktails.R
import com.example.cocktails.adapters.recyclers.items.converter.ItemConverter
import com.example.cocktails.extensions.Event
import com.example.cocktails.networking.converters.ResponseConverter
import com.example.cocktails.networking.response.CocktailResponse
import com.example.cocktails.networking.service.CocktailService
import com.example.cocktails.persistance.models.CocktailDetailsPersist
import com.example.cocktails.persistance.services.CocktailDetailsService
import com.example.cocktails.ui.fragments.base.BaseVM
import com.example.cocktails.util.COCKTAIL_IMAGE_URL_INITIAL_VALUE
import com.example.cocktails.util.NetworkUtils
import com.example.cocktails.util.constructCocktailRecipe
import io.reactivex.functions.Consumer
import javax.inject.Inject

class HomeVM @Inject constructor() : BaseVM() {

    @Inject
    lateinit var cocktailsNetworkService: CocktailService
    @Inject
    lateinit var cocktailPersistService: CocktailDetailsService

    var cocktailImageUrl = MutableLiveData<String>().apply { value = COCKTAIL_IMAGE_URL_INITIAL_VALUE }
    var cocktailName = MutableLiveData<String>()
    var randomCocktailCardVisibility = MutableLiveData<Boolean>().apply { value = true }
    private lateinit var cocktailResponse: CocktailResponse
    private var randomCocktail: CocktailDetailsPersist? = null

    override fun onCreate() {
        if (NetworkUtils.isNetworkAvailable() != null) {
            loadRandomCocktail()
        } else {
            error.value = Event(R.string.alert_dialog_random_cocktail_error_message)
            randomCocktailCardVisibility.value = false
            progressBarVisibility.value = false
        }
    }

    private fun loadRandomCocktail() {
        progressBarVisibility.value = true
        subscribeSingle(
            cocktailsNetworkService.getRandomCocktail(),
            Consumer {
                setCocktailImage(ResponseConverter.convertCocktailDetailsResponse(it).first())
                cocktailResponse = it.drinks.first()
            },
            Consumer { showError(it) }
        )
    }

    private fun setCocktailImage(cocktail: CocktailDetailsPersist) {
        if (TextUtils.isEmpty(cocktail.imageUrl)) {
            randomCocktailCardVisibility.value = false
            progressBarVisibility.value = false
        } else {
            checkDb(cocktail)
        }
    }

    private fun checkDb(cocktail: CocktailDetailsPersist) {
        subscribeSingle(
            cocktailPersistService.getCocktailById(cocktail.drinkId),
            Consumer { result ->
                if (cocktail.instructions?.isNotEmpty()!!) {
                    result.instructions = cocktail.instructions
                    cocktailPersistService.update(result)
                }

                setRandomCocktail(result)
                randomCocktail = result
            },
            Consumer {
                setRandomCocktail(cocktail)
                randomCocktail = cocktail
            }
        )
        progressBarVisibility.value = false
    }

    private fun setRandomCocktail(cocktail: CocktailDetailsPersist) {
        cocktailImageUrl.postValue(cocktail.imageUrl)
        cocktailName.postValue(cocktail.name)
    }

    private fun showError(throwable: Throwable) {
        error.value = Event(R.string.alert_dialog_random_cocktail_error_message)
        randomCocktailCardVisibility.value = false
        progressBarVisibility.value = false
    }

    fun onRandomCocktailClick() {
        newDestinationEvent.value = Event(
            HomeFragmentDirections.actionHomeFragmentToCocktailDetailsFragment(
                ItemConverter.convertCocktailPersistToRecyclerItem(this.randomCocktail!!),
                constructCocktailRecipe(
                    ResponseConverter.extractCocktailIngredientsAndMeasuresFromResponse(cocktailResponse)
                )
            )
        )
    }

    fun onCocktailCollectionClick(collectionId: Int) {
        when (collectionId) {
            R.id.card_view_home_favorites -> {
                newDestinationEvent.value = Event(
                    HomeFragmentDirections.actionHomeFragmentToBaseCollectionFragment(true)
                )
            }
            else -> {
                newDestinationEvent.value = Event(
                    HomeFragmentDirections.actionHomeFragmentToBaseCollectionFragment(false)
                )
            }
        }
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
    fun onResume(){
        if (randomCocktail != null) {
            checkDb(this.randomCocktail!!)
        }
    }
}