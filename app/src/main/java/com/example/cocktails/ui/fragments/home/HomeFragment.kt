package com.example.cocktails.ui.fragments.home

import com.example.cocktails.R
import com.example.cocktails.databinding.FragmentHomeBinding
import com.example.cocktails.ui.fragments.base.BaseFragment


class HomeFragment : BaseFragment<HomeVM, FragmentHomeBinding>() {

    override fun getLayoutId(): Int = R.layout.fragment_home

    override val viewModelClass = HomeVM::class
}
