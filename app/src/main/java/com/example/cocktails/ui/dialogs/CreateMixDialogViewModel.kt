package com.example.cocktails.ui.dialogs

import androidx.databinding.ObservableArrayList
import androidx.lifecycle.MutableLiveData
import com.example.cocktails.R
import com.example.cocktails.adapters.recyclers.items.CocktailRecyclerItem
import com.example.cocktails.adapters.recyclers.items.MyMixIngredientRecyclerItem
import com.example.cocktails.adapters.recyclers.items.converter.ItemConverter.convertCocktailPersistToRecyclerItem
import com.example.cocktails.extensions.Event
import com.example.cocktails.persistance.models.CocktailDetailsPersist
import com.example.cocktails.persistance.models.IngredientPersist
import com.example.cocktails.persistance.services.CocktailDetailsService
import com.example.cocktails.persistance.services.IngredientsService
import com.example.cocktails.ui.fragments.base.BaseVM
import com.example.cocktails.util.ALCOHOLIC_DRINK_ID
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.functions.Action
import io.reactivex.internal.operators.completable.CompletableFromAction
import io.reactivex.schedulers.Schedulers
import java.util.*
import javax.inject.Inject

class CreateMixDialogViewModel @Inject constructor() : BaseVM() {

    @Inject
    lateinit var cocktailPersistService: CocktailDetailsService
    @Inject
    lateinit var ingredientsPersistService: IngredientsService

    var addedIngredients: ObservableArrayList<MyMixIngredientRecyclerItem> = ObservableArrayList()
    var isCancelClicked = MutableLiveData<Boolean>()
    var cocktailName = MutableLiveData<String>()
    var cocktailInstructions = MutableLiveData<String>()
    var completedCocktail = MutableLiveData<CocktailRecyclerItem>()
    var photoLocation = MutableLiveData<String>()
    var isImageClicked = MutableLiveData<Boolean>()
    var cocktailError = MutableLiveData<Event<Int>>()
    private var disposable: Disposable? = null

    override fun onCreate() {
        addedIngredients.add(MyMixIngredientRecyclerItem())
    }

    fun onDeleteItemClick(item: MyMixIngredientRecyclerItem) {
        if (addedIngredients.size > 1) {
            addedIngredients.remove(item)
        } else {
            cocktailError.value = Event(R.string.create_mix_ingredients_error)
        }
    }

    fun onAddItemClick() {
        addedIngredients.add(MyMixIngredientRecyclerItem())
    }

    fun onCancelClicked() {
        isCancelClicked.value = true
    }

    fun onSaveClicked() {
        if (hasNoErrors()) {
            val ingredients = mutableListOf<IngredientPersist>()
            persistIngredients(ingredients)
            persistCocktail()
        }
    }

    private fun hasNoErrors(): Boolean {
        return when {
            addedIngredients.first().ingredient.isNullOrEmpty() -> {
                cocktailError.value = Event(R.string.dialog_create_mix_save_empty_cocktail_error)
                false
            }
            cocktailName.value.isNullOrEmpty() -> {
                cocktailError.value = Event(R.string.dialog_create_mix_no_name_error)
                false
            }
            else -> true
        }
    }

    private fun persistCocktail() {
        if (photoLocation.value.isNullOrEmpty()) {
            photoLocation.value = ""
        }

        val cocktail = CocktailDetailsPersist(
            cocktailName.value!!,
            Objects.hash(cocktailName.value!!, Calendar.getInstance().time),
            ALCOHOLIC_DRINK_ID,
            cocktailInstructions.value,
            photoLocation.value,
            isFavorited = false,
            isCreatedByMe = true
        )

        val insert = CompletableFromAction(Action { cocktailPersistService.insert(cocktail) })
        disposable = insert
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { completedCocktail.value = convertCocktailPersistToRecyclerItem(cocktail)
                    disposable?.dispose()
                },
                {
                    cocktailError.value = Event(R.string.dialog_create_mix_db_error)
                }
            )
    }

    private fun persistIngredients(ingredients: MutableList<IngredientPersist>) {
        addedIngredients.forEach { ingredient ->
            ingredients.add(
                IngredientPersist(
                    null,
                    ingredient.ingredient!!,
                    ingredient.measure!!,
                    cocktailName.value!!
                )
            )
        }

        ingredientsPersistService.insertAll(ingredients)
    }

    fun onPickImageClick() {
        isImageClicked.value = true
    }
}