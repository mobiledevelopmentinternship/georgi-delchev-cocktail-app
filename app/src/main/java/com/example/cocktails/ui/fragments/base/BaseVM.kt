package com.example.cocktails.ui.fragments.base

import androidx.lifecycle.*
import androidx.navigation.NavDirections
import com.example.cocktails.extensions.Event
import com.example.cocktails.util.RxUtils
import io.reactivex.Completable
import io.reactivex.Single
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.functions.Action
import io.reactivex.functions.Consumer

abstract class BaseVM : ViewModel(), LifecycleObserver {

    private var compositeDisposable: CompositeDisposable? = null
    var cardViewId = MutableLiveData<Int>()

    var progressBarVisibility = MutableLiveData<Boolean>()
    var error = MutableLiveData<Event<Int>>()
    val newDestinationEvent = MutableLiveData<Event<NavDirections>>()

    @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
    protected abstract fun onCreate()

    protected fun <T> subscribeSingle(
        observable: Single<T>,
        onSuccess: Consumer<T>,
        onError: Consumer<Throwable>
    ) = getCompositeDisposable().add(
        observable.compose(RxUtils.applySingleSchedulers())
            .subscribe(onSuccess, onError)
    )

    protected fun subscribeCompletable(
        observable: Completable
    ) = getCompositeDisposable()
        .add(
            observable.compose(RxUtils.applyCompletableSchedulers())
                .subscribe()
        )

    protected fun subscribeCompletable(
        observable: Completable,
        onComplete: Action
    ) = getCompositeDisposable()
        .add(
            observable.compose(RxUtils.applyCompletableSchedulers())
                .subscribe(onComplete)
        )

    private fun getCompositeDisposable(): CompositeDisposable {
        if (compositeDisposable == null || compositeDisposable!!.isDisposed) {
            compositeDisposable = CompositeDisposable()
        }

        return compositeDisposable as CompositeDisposable
    }

    fun setNewDestination(direction: NavDirections) = newDestinationEvent
        .apply { value = Event(direction) }

    override fun onCleared() {
        getCompositeDisposable().dispose()
        super.onCleared()
    }
}