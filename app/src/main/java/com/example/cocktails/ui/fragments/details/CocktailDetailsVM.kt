package com.example.cocktails.ui.fragments.details

import androidx.lifecycle.MutableLiveData
import com.example.cocktails.R
import com.example.cocktails.persistance.models.CocktailDetailsPersist
import com.example.cocktails.persistance.services.CocktailDetailsService
import com.example.cocktails.ui.fragments.base.BaseVM
import io.reactivex.functions.Consumer
import javax.inject.Inject

class CocktailDetailsVM @Inject constructor() : BaseVM() {

    @Inject
    lateinit var cocktailPersistService: CocktailDetailsService

    val selectedCocktail = MutableLiveData<CocktailDetailsPersist>()
    val selectedCocktailRecipe = MutableLiveData<String>()
    var iconId = MutableLiveData<Int>()
    var isAddedToFavorites = MutableLiveData<Boolean>()
    var isCreatedByMe = MutableLiveData<Boolean>()

    override fun onCreate() {
        progressBarVisibility.value = false
    }

    fun onFavoritesClicked() {
        selectedCocktail.value?.isFavorited = !selectedCocktail.value?.isFavorited!!
        setFavoredIcon()
        isAddedToFavorites.value = selectedCocktail.value!!.isFavorited
        subscribeSingle(
            cocktailPersistService.getCocktailById(selectedCocktail.value?.drinkId!!),
            Consumer {
                cocktailPersistService.update(selectedCocktail.value!!)
            },
            Consumer {
                cocktailPersistService.insert(selectedCocktail.value!!)
            }
        )
    }

    private fun setFavoredIcon() {
        if (selectedCocktail.value?.isFavorited!!) {
            iconId.value = R.drawable.ic_star_full
        } else {
            iconId.value = R.drawable.ic_star_empty
        }
    }

    fun setSelectedCocktail(cocktail: CocktailDetailsPersist) {
        selectedCocktail.value = cocktail
        isCreatedByMe.value = selectedCocktail.value?.isCreatedByMe
        iconId.value = if (selectedCocktail.value?.isFavorited!!)
            R.drawable.ic_star_full else R.drawable.ic_star_empty
    }
}