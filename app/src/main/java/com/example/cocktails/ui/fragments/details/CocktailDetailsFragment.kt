package com.example.cocktails.ui.fragments.details

import android.annotation.SuppressLint
import android.os.Bundle
import android.text.TextUtils
import android.view.View
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.navigation.fragment.navArgs
import com.example.cocktails.R
import com.example.cocktails.adapters.recyclers.items.converter.ItemConverter
import com.example.cocktails.databinding.FragmentCocktailDetailsBinding
import com.example.cocktails.persistance.models.CocktailDetailsPersist
import com.example.cocktails.ui.fragments.base.BaseFragment

class CocktailDetailsFragment : BaseFragment<CocktailDetailsVM, FragmentCocktailDetailsBinding>() {

    override val viewModelClass = CocktailDetailsVM::class

    override fun getLayoutId(): Int = R.layout.fragment_cocktail_details

    private val arguments by navArgs<CocktailDetailsFragmentArgs>()
    private lateinit var selectedCocktail: CocktailDetailsPersist

    @SuppressLint("RestrictedApi")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.selectedCocktailRecipe.value = arguments.cocktailRecipe
        selectedCocktail = ItemConverter.convertRecyclerItemToCocktailPersist(
            arguments.receivedCocktail
        )
        viewModel.setSelectedCocktail(selectedCocktail)

        setActionBarTitle(arguments.receivedCocktail.name)
        observeFavoritesFab()
    }

    private fun observeFavoritesFab() = viewModel.isAddedToFavorites.observe(viewLifecycleOwner,
        Observer { isAdded ->
            if (isAdded) {
                Toast.makeText(
                    requireContext(), TextUtils.concat(
                        getString(R.string.toast_congrats_message), " ",
                        selectedCocktail.name, " ", getString(R.string.toast_to_favorites)
                    ), Toast.LENGTH_SHORT
                ).show()
            } else {
                Toast.makeText(
                    requireContext(), TextUtils.concat(
                        getString(R.string.toast_cocktail_removed), " ",
                        selectedCocktail.name, " ", getString(R.string.toast_cocktail_removed_from_favorites)
                    ), Toast.LENGTH_SHORT
                ).show()
            }
        })
}