package com.example.cocktails.ui.fragments.favorites.mymixes

import androidx.lifecycle.MutableLiveData
import com.example.cocktails.ui.fragments.base.BaseCocktailsVM
import javax.inject.Inject

class MyCocktailMixesVM @Inject constructor() : BaseCocktailsVM() {

    var fabClick = MutableLiveData<Boolean>()

    override fun onCreate() {
        getCreatedCocktailsFromDb()
    }

    fun getCreatedCocktailsFromDb() = cocktailDetailsPersistService.getCreatedCocktails()

    fun onFabCreateCocktailClicked() {
        fabClick.value = true
    }
}