package com.example.cocktails.ui.fragments.menu.nonalcoholic

import com.example.cocktails.R
import com.example.cocktails.extensions.Event
import com.example.cocktails.persistance.models.CocktailDetailsPersist
import com.example.cocktails.ui.fragments.base.BaseCocktailsVM
import com.example.cocktails.util.NON_ALCOHOLIC_COCKTAILS_FROM_API_COUNT
import com.example.cocktails.util.NetworkUtils
import io.reactivex.functions.Consumer
import javax.inject.Inject

class NonAlcoholicCocktailsVM @Inject constructor() : BaseCocktailsVM() {

    override fun onCreate() {
        getNonAlcoholicCocktailsFromDb()
    }

    private fun getNonAlcoholicCocktailsFromDb() {
        progressBarVisibility.value = true
        subscribeSingle(
            cocktailDetailsPersistService.getAllNonAlcoholicCocktails(),
            Consumer { result ->
                if (result.size < NON_ALCOHOLIC_COCKTAILS_FROM_API_COUNT) {
                    getNonAlcoholicCocktailsFromApi()
                } else {
                    loadNonAlcoholicCocktails(result)
                }
            },
            Consumer {
                if (NetworkUtils.isNetworkAvailable() != null) {
                    getNonAlcoholicCocktailsFromApi()
                } else {
                    error.value = Event(R.string.alert_dialog_random_cocktail_error_message)
                    progressBarVisibility.value = false
                }
            }
        )
    }

    private fun loadNonAlcoholicCocktails(result: List<CocktailDetailsPersist>) {
        if (result.isNotEmpty()) {
            processResults(result)
            progressBarVisibility.value = false
        } else {
            getNonAlcoholicCocktailsFromApi()
        }
    }
}