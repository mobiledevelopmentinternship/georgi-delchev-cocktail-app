package com.example.cocktails.ui.dialogs


import android.content.Intent
import android.os.Bundle
import android.view.View
import android.view.View.VISIBLE
import android.view.ViewGroup
import androidx.lifecycle.Observer
import com.example.cocktails.R
import com.example.cocktails.databinding.FragmentDialogCreateMixBinding
import com.example.cocktails.extensions.showErrorDialog
import com.example.cocktails.ui.dialogs.base.BaseDialogFragment
import com.example.cocktails.util.CREATED_COCKTAIL_BUNDLE_KEY
import com.example.cocktails.util.CREATE_MIX_DIALOG_REQUEST_CODE
import com.vansuita.pickimage.bundle.PickSetup
import com.vansuita.pickimage.dialog.PickImageDialog
import kotlin.reflect.KClass


class CreateMixDialogFragment : BaseDialogFragment<CreateMixDialogViewModel, FragmentDialogCreateMixBinding>() {

    override val viewModelClass: KClass<CreateMixDialogViewModel> = CreateMixDialogViewModel::class

    override fun getLayoutId(): Int = R.layout.fragment_dialog_create_mix

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        dialog?.window?.setLayout(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.MATCH_PARENT
        )
        setStyle(STYLE_NO_FRAME, R.style.FullScreenDialogStyle)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        observeCancelClicks()
        observeCreatedCocktail()
        observeImagePickClicks()
        observeError()
    }

    private fun observeError() = viewModel.cocktailError.observe(this, Observer {
        showErrorDialog(it.getContent()!!)
    })

    private fun observeImagePickClicks() = viewModel.isImageClicked.observe(this, Observer { isClicked ->
        if (isClicked) {
            PickImageDialog.build(PickSetup())
                .setOnPickResult {result ->
                    if (result.error == null) {
                        viewModel.photoLocation.value = result.path
                        binding.imgDialogCreateMix.setImageURI(result.uri)
                        binding.imgDialogCreateMix.visibility = VISIBLE
                    }
                }
                .show(fragmentManager)

            viewModel.isImageClicked.value = false
        }
    })

    private fun observeCreatedCocktail() = viewModel.completedCocktail.observe(this, Observer {
        if (targetFragment != null) {
            val intent = Intent()
            intent.putExtra(CREATED_COCKTAIL_BUNDLE_KEY, it)
            targetFragment!!.onActivityResult(targetRequestCode, CREATE_MIX_DIALOG_REQUEST_CODE, intent)
            dismiss()
        }
    })

    private fun observeCancelClicks() = viewModel.isCancelClicked.observe(this, Observer { isClicked ->
        if (isClicked) {
            dismiss()
        }
    })
}