package com.example.cocktails.ui.fragments.menu.nonalcoholic

import android.os.Bundle
import com.example.cocktails.R
import com.example.cocktails.databinding.FragmentCocktailsListBinding
import com.example.cocktails.ui.fragments.base.BaseCocktailCollectionFragment
import com.example.cocktails.util.FRAGMENT_TAG_BUNDLE_KEY

class NonAlcoholicCocktailsCollectionFragment :
    BaseCocktailCollectionFragment<NonAlcoholicCocktailsVM, FragmentCocktailsListBinding>() {

    override val viewModelClass = NonAlcoholicCocktailsVM::class

    override fun getLayoutId(): Int = R.layout.fragment_cocktail_collection

    companion object {
        fun newInstance(tag: String): NonAlcoholicCocktailsCollectionFragment {
            val nonAlcoholicCocktailsCollectionFragment = NonAlcoholicCocktailsCollectionFragment()
            val bundle = Bundle()
            bundle.putString(FRAGMENT_TAG_BUNDLE_KEY, tag)
            nonAlcoholicCocktailsCollectionFragment.arguments = bundle

            return nonAlcoholicCocktailsCollectionFragment
        }
    }
}
