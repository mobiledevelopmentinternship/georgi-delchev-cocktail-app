package com.example.cocktails.ui.fragments.favorites.favorites

import com.example.cocktails.ui.fragments.base.BaseCocktailsVM
import javax.inject.Inject

class FavoriteCocktailsVM @Inject constructor() : BaseCocktailsVM() {

    override fun onCreate() {
        getFavoriteCocktails()
    }

    fun getFavoriteCocktails() = cocktailDetailsPersistService.getFavoriteCocktails()
}