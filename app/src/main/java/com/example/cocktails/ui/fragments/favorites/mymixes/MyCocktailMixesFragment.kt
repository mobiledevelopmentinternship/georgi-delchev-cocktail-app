package com.example.cocktails.ui.fragments.favorites.mymixes

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import com.example.cocktails.R
import com.example.cocktails.adapters.recyclers.items.CocktailRecyclerItem
import com.example.cocktails.adapters.recyclers.items.converter.ItemConverter
import com.example.cocktails.databinding.FragmentMyMixesBinding
import com.example.cocktails.ui.dialogs.CreateMixDialogFragment
import com.example.cocktails.ui.fragments.base.BaseCocktailCollectionFragment
import com.example.cocktails.util.CREATED_COCKTAIL_BUNDLE_KEY
import com.example.cocktails.util.CREATE_MIX_DIALOG_REQUEST_CODE
import com.example.cocktails.util.CREATE_MIX_DIALOG_TAG
import com.example.cocktails.util.FRAGMENT_TAG_BUNDLE_KEY

class MyCocktailMixesFragment : BaseCocktailCollectionFragment<MyCocktailMixesVM, FragmentMyMixesBinding>() {

    override val viewModelClass = MyCocktailMixesVM::class

    override fun getLayoutId(): Int = R.layout.fragment_my_mixes

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        observeCreatedCocktails()
        observeFabClicks()
    }

    private fun observeCreatedCocktails() = viewModel.getCreatedCocktailsFromDb().observe(viewLifecycleOwner,
        Observer {
            val convertedItems = mutableListOf<CocktailRecyclerItem>()
            it.forEach { cocktail ->
                convertedItems.add(ItemConverter.convertCocktailPersistToRecyclerItem(cocktail))
            }
            updateCocktailsListContent(convertedItems)
        })

    private fun observeFabClicks() = viewModel.fabClick.observe(viewLifecycleOwner,
        Observer { isClicked ->
            if (isClicked) {
                val dialog = CreateMixDialogFragment()
                dialog.setTargetFragment(this, CREATE_MIX_DIALOG_REQUEST_CODE)
                dialog.show(fragmentManager!!, CREATE_MIX_DIALOG_TAG)
                viewModel.fabClick.value = false
            }
        })

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (data != null) {
            val result: CocktailRecyclerItem = data.getParcelableExtra(CREATED_COCKTAIL_BUNDLE_KEY)
            viewModel.cocktailsList.add(result)
        }
    }

    companion object {
        fun newInstance(tag: String): MyCocktailMixesFragment {
            val myCocktailMixesFragment = MyCocktailMixesFragment()
            val bundle = Bundle()
            bundle.putString(FRAGMENT_TAG_BUNDLE_KEY, tag)
            myCocktailMixesFragment.arguments = bundle

            return myCocktailMixesFragment
        }
    }
}
