package com.example.cocktails.ui.fragments.base

import android.text.TextUtils
import android.view.View
import androidx.databinding.ObservableArrayList
import androidx.lifecycle.MutableLiveData
import com.example.cocktails.R
import com.example.cocktails.adapters.recyclers.items.CocktailRecyclerItem
import com.example.cocktails.adapters.recyclers.items.converter.ItemConverter
import com.example.cocktails.adapters.recyclers.items.converter.ItemConverter.convertCocktailPersistToRecyclerItem
import com.example.cocktails.extensions.Event
import com.example.cocktails.networking.converters.ResponseConverter
import com.example.cocktails.networking.response.DrinkResponse
import com.example.cocktails.networking.service.CocktailService
import com.example.cocktails.persistance.models.CocktailDetailsPersist
import com.example.cocktails.persistance.models.IngredientPersist
import com.example.cocktails.persistance.services.CocktailDetailsService
import com.example.cocktails.persistance.services.IngredientsService
import com.example.cocktails.ui.fragments.base.cocktaillists.CocktailsListFragmentDirections
import com.example.cocktails.util.constructCocktailRecipe
import io.reactivex.Completable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.functions.Action
import io.reactivex.functions.Consumer
import io.reactivex.internal.operators.completable.CompletableFromAction
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

abstract class BaseCocktailsVM : BaseVM() {

    var menuOptionsClicked = MutableLiveData<Pair<View, CocktailRecyclerItem>>()
    var isAddedToFavorites: MutableLiveData<Boolean>? = MutableLiveData()
    var isRemovedFromFavorites: MutableLiveData<Boolean>? = MutableLiveData()
    var isDeletedFromMyMixes: MutableLiveData<Boolean>? = MutableLiveData()
    private var favoritesCompletable: Disposable? = null
    private var cocktailClickPair: Pair<View, CocktailRecyclerItem>? = null

    @Inject
    protected lateinit var cocktailDetailsPersistService: CocktailDetailsService
    @Inject
    protected lateinit var cocktailNetworkService: CocktailService
    @Inject
    protected lateinit var ingredientPersistService: IngredientsService

    private var completable: Disposable? = null

    var cocktailsList = ObservableArrayList<CocktailRecyclerItem>()
    private var selectedCocktailId = 0

    fun onCocktailClicked(cocktail: CocktailRecyclerItem) {
        if (selectedCocktailId != cocktail.id) {
            getCocktailFromDb(cocktail.id)
            selectedCocktailId = cocktail.id
        }
    }

    private fun getCocktailFromDb(cocktailId: Int) {
        subscribeSingle(
            cocktailDetailsPersistService.getCocktailById(cocktailId),
            Consumer {
                getCocktailIngredients(convertCocktailPersistToRecyclerItem(it))
            },
            Consumer {
                getCocktailDetailsFromApi(cocktailId)
            }
        )
    }

    private fun getCocktailDetailsFromApi(cocktailId: Int) {
        subscribeSingle(
            cocktailNetworkService.getFullCocktailDetails(cocktailId),
            Consumer {
                val fullCocktailDetails = ResponseConverter.convertCocktailDetailsResponse(it).first()
                cocktailDetailsPersistService.update(fullCocktailDetails)
                getCocktailIngredients(convertCocktailPersistToRecyclerItem(fullCocktailDetails))
            },
            Consumer {
                error.value = Event(R.string.cocktail_details_loading_error)
                progressBarVisibility.value = false
            }
        )
    }

    private fun getCocktailIngredients(cocktail: CocktailRecyclerItem) {
        subscribeSingle(
            cocktailDetailsPersistService.getCocktailIngredients(cocktail.name),
            Consumer { onCocktailIngredientsQuerySuccess(it, cocktail) },
            Consumer { getCocktailFromApi(cocktail) }
        )
    }

    private fun onCocktailIngredientsQuerySuccess(
        ingredients: List<IngredientPersist>,
        cocktail: CocktailRecyclerItem
    ) {
        if (ingredients.isNotEmpty()) {
            navigateWithCocktailAndIngredients(cocktail, ingredients)
        } else {
            getCocktailFromApi(cocktail)
        }
    }

    private fun getCocktailFromApi(cocktail: CocktailRecyclerItem) {
        subscribeSingle(
            cocktailNetworkService.getFullCocktailDetails(cocktail.id),
            Consumer { onCocktailDetailsRequestSuccess(it, cocktail) },
            Consumer {
                error.value = Event(R.string.alert_dialog_random_cocktail_error_message)
                progressBarVisibility.value = false
            }
        )
    }

    private fun onCocktailDetailsRequestSuccess(
        response: DrinkResponse,
        cocktail: CocktailRecyclerItem
    ) {
        navigateWithCocktailAndIngredients(
            cocktail,
            ResponseConverter.extractCocktailIngredientsAndMeasuresFromResponse(response.drinks.first())
        )
        selectedCocktailId = 0
    }

    private fun navigateWithCocktailAndIngredients(
        cocktail: CocktailRecyclerItem,
        ingredients: List<IngredientPersist>
    ) {
        newDestinationEvent.value = Event(
            CocktailsListFragmentDirections.actionBaseCollectionFragmentToCocktailDetailsFragment(
                cocktail,
                constructCocktailRecipe(ingredients)
            )
        )
        selectedCocktailId = 0
    }

    fun onCocktailOptionsClicked(cocktail: CocktailRecyclerItem, view: View) {
        cocktailClickPair = Pair(view, cocktail)
        updateCocktailDetails(view, cocktail.id)
    }

    private fun updateCocktailDetails(view: View, cocktailId: Int) {
        subscribeSingle(
            cocktailDetailsPersistService.getCocktailById(cocktailId),
            Consumer {
                val converted = convertCocktailPersistToRecyclerItem(it)
                menuOptionsClicked.value = Pair(view, converted)
                cocktailClickPair = null
            },
            Consumer {
                menuOptionsClicked.value = cocktailClickPair
                cocktailClickPair = null
            }
        )
    }

    protected fun getAlcoholicCocktailsFromApi() {
        subscribeSingle(
            cocktailNetworkService.getAlcoholicCocktails(),
            Consumer { result ->
                loadCocktailsFromApi(
                    ResponseConverter.convertAlcoholicCocktailsResponse(result)
                )
                saveCocktailIngredients(result)
            },
            Consumer {
                error.value = Event(R.string.alert_dialog_random_cocktail_error_message)
                progressBarVisibility.value = false
            }
        )
    }

    private fun saveCocktailIngredients(result: DrinkResponse?) {
        for (drink in result?.drinks!!) {
            val ingredients = ResponseConverter.extractCocktailIngredientsAndMeasuresFromResponse(drink)
            ingredientPersistService.insertAll(ingredients)
        }
    }

    private fun loadCocktailsFromApi(convertAlcoholicCocktailsResponse: MutableList<CocktailDetailsPersist>) {
        saveCocktailsInDb(convertAlcoholicCocktailsResponse)
    }

    private fun convertCocktails(convertAlcoholicCocktailsResponse: MutableList<CocktailDetailsPersist>) {
        convertAlcoholicCocktailsResponse.forEach { cocktail ->
            cocktailsList.add(convertCocktailPersistToRecyclerItem(cocktail))
        }
        progressBarVisibility.value = false
    }

    private fun saveCocktailsInDb(convertedAlcoholicCocktailsResponse: MutableList<CocktailDetailsPersist>) {
        completable = Completable.fromAction {
            cocktailDetailsPersistService.insertAll(convertedAlcoholicCocktailsResponse)
        }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                {
                    completable?.dispose()
                    convertCocktails(convertedAlcoholicCocktailsResponse)
                },
                { completable?.dispose() }
            )
    }

    protected fun getNonAlcoholicCocktailsFromApi() {
        subscribeSingle(
            cocktailNetworkService.getNonAlcoholicCocktails(),
            Consumer { result ->
                loadCocktailsFromApi(
                    ResponseConverter.convertNonAlcoholicCocktailsResponse(result)
                )
                saveCocktailIngredients(result)
            },
            Consumer {
                error.value = Event(R.string.alert_dialog_random_cocktail_error_message)
                progressBarVisibility.value = false
            }
        )
    }

    fun saveToFavorites(cocktail: CocktailDetailsPersist) {
        subscribeSingle(
            cocktailDetailsPersistService.getAllFavoriteCocktails(),
            Consumer {
                if (checkFavorites(it, cocktail)) {
                    isAddedToFavorites?.value = true
                } else {
                    addToFavorites(Throwable(), cocktail)
                }
            },
            Consumer { addToFavorites(it, cocktail) }
        )
    }

    private fun checkFavorites(
        favoriteCocktails: List<CocktailDetailsPersist>,
        cocktail: CocktailDetailsPersist
    ): Boolean {
        favoriteCocktails.forEach {
            if (TextUtils.equals(it.name, cocktail.name)) {
                return true
            }
        }
        return false
    }

    private fun addToFavorites(
        throwable: Throwable,
        cocktail: CocktailDetailsPersist
    ) {
        cocktail.isFavorited = true

        favoritesCompletable = Completable.fromAction {
            cocktailDetailsPersistService.update(cocktail)
        }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doFinally { favoritesCompletable?.dispose() }
            .subscribe(
                {
                    isAddedToFavorites?.value = false
                    favoritesCompletable?.dispose()
                },
                {
                    favoritesCompletable?.dispose()
                    error.value = Event(R.string.alert_dialog_favorites_error)
                }
            )
    }

    fun removeFromFavorites(cocktail: CocktailDetailsPersist) {
        cocktail.isFavorited = false
        favoritesCompletable = CompletableFromAction(Action { cocktailDetailsPersistService.update(cocktail) })
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doFinally { favoritesCompletable?.dispose() }
            .subscribe(
                {
                    isRemovedFromFavorites?.value = true
                },
                {
                    isRemovedFromFavorites?.value = false
                }
            )
    }

    protected fun processResults(result: List<CocktailDetailsPersist>) {
        val sortedDrinks = result.sortedBy { it.name }
        sortedDrinks.forEach { cocktail ->
            cocktailsList.add(convertCocktailPersistToRecyclerItem(cocktail))
        }
    }

    fun deleteCreatedCocktail(cocktail: CocktailRecyclerItem) {
        favoritesCompletable = CompletableFromAction(Action {
            cocktailDetailsPersistService.delete(
                ItemConverter.convertRecyclerItemToCocktailPersist(cocktail)
            )
        })
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doFinally { favoritesCompletable?.dispose() }
            .subscribe(
                Action {
                    isDeletedFromMyMixes?.value = true
                }
            )
    }
}