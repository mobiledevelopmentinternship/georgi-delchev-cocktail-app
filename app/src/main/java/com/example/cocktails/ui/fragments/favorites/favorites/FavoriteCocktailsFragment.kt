package com.example.cocktails.ui.fragments.favorites.favorites

import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import com.example.cocktails.R
import com.example.cocktails.adapters.recyclers.items.CocktailRecyclerItem
import com.example.cocktails.adapters.recyclers.items.converter.ItemConverter
import com.example.cocktails.databinding.FragmentCocktailsListBinding
import com.example.cocktails.ui.fragments.base.BaseCocktailCollectionFragment
import com.example.cocktails.util.FRAGMENT_TAG_BUNDLE_KEY

class FavoriteCocktailsFragment : BaseCocktailCollectionFragment<FavoriteCocktailsVM, FragmentCocktailsListBinding>() {

    override val viewModelClass = FavoriteCocktailsVM::class
    override fun getLayoutId(): Int = R.layout.fragment_cocktail_collection

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.getFavoriteCocktails().observe(viewLifecycleOwner,
            Observer {
                val convertedItems = mutableListOf<CocktailRecyclerItem>()
                it.forEach { cocktail ->
                    convertedItems.add(ItemConverter.convertCocktailPersistToRecyclerItem(cocktail))
                }
                updateCocktailsListContent(convertedItems)
            })
    }

    companion object {
        fun newInstance(tag: String): FavoriteCocktailsFragment {
            val favoriteCocktailsFragment = FavoriteCocktailsFragment()
            val bundle = Bundle()
            bundle.putString(FRAGMENT_TAG_BUNDLE_KEY, tag)
            favoriteCocktailsFragment.arguments = bundle

            return favoriteCocktailsFragment
        }
    }
}