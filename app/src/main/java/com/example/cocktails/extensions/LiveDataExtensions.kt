package com.example.cocktails.extensions

import android.os.Bundle

class Event<out T>(private val content: T, val bundle: Bundle? = null) {

    var hasBeenHandled = false
        private set

    /**
     * Returns the content if the boolean flag is currently false.
     * Returns null if true.
     */
    fun getContent(): T? =
            if (hasBeenHandled) {
                null
            } else {
                hasBeenHandled = true
                content
            }
}