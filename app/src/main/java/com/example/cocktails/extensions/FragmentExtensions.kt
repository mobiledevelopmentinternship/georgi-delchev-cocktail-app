package com.example.cocktails.extensions

import android.app.AlertDialog
import androidx.fragment.app.Fragment
import com.example.cocktails.R

fun Fragment.showErrorDialog(
    messageId: Int
) {
    val dialog = AlertDialog.Builder(context)
        .setTitle(getString(R.string.alert_dialog_error_title))
        .setMessage(getString(messageId))
        .setPositiveButton(getString(R.string.alert_dialog_positive_answer))
        { dialog, _ -> dialog.dismiss() }
        .create()
    dialog.show()
}