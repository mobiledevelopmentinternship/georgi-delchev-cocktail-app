package com.example.cocktails.dagger.modules

import android.app.Application
import android.content.Context
import com.example.cocktails.persistance.executors.AppExecutor
import dagger.Module
import dagger.Provides
import java.util.concurrent.Executors

@Module
class AppModule(private val application: Application) {

    @Provides
    fun provideApplication() = application

    @Provides
    fun provideContext(): Context = application

    @Provides
    fun provideAppExecutor() = AppExecutor(Executors.newSingleThreadExecutor())
}