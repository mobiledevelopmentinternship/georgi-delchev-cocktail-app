package com.example.cocktails.dagger.modules

import android.app.Application
import com.example.cocktails.BuildConfig
import com.example.cocktails.networking.api.CocktailApi
import com.example.cocktails.util.CONNECTION_TIMEOUT
import com.readystatesoftware.chuck.ChuckInterceptor
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module
class NetworkModule {

    @Singleton
    @Provides
    fun provideRetrofit(
        okHttpClient: OkHttpClient
    ): Retrofit = Retrofit.Builder()
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .addConverterFactory(GsonConverterFactory.create())
        .baseUrl(BuildConfig.BASE_URL)
        .client(okHttpClient)
        .build()

    @Singleton
    @Provides
    fun provideOkHttpClient(application: Application): OkHttpClient =
        OkHttpClient.Builder()
            .addInterceptor(provideChuckInterceptor(application))
            .writeTimeout(CONNECTION_TIMEOUT, TimeUnit.MILLISECONDS)
            .connectTimeout(CONNECTION_TIMEOUT, TimeUnit.MILLISECONDS)
            .readTimeout(CONNECTION_TIMEOUT, TimeUnit.MILLISECONDS)
            .build()

    @Singleton
    @Provides
    fun provideChuckInterceptor(application: Application) = ChuckInterceptor(application)

    @Singleton
    @Provides
    fun provideCocktailApi(retrofit: Retrofit): CocktailApi = retrofit.create(CocktailApi::class.java)
}