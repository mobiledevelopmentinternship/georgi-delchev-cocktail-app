package com.example.cocktails.dagger.modules

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.cocktails.dagger.ViewModelFactory
import com.example.cocktails.dagger.ViewModelKey
import com.example.cocktails.ui.dialogs.CreateMixDialogFragment
import com.example.cocktails.ui.dialogs.CreateMixDialogViewModel
import com.example.cocktails.ui.fragments.details.CocktailDetailsFragment
import com.example.cocktails.ui.fragments.details.CocktailDetailsVM
import com.example.cocktails.ui.fragments.favorites.favorites.FavoriteCocktailsFragment
import com.example.cocktails.ui.fragments.favorites.favorites.FavoriteCocktailsVM
import com.example.cocktails.ui.fragments.favorites.mymixes.MyCocktailMixesFragment
import com.example.cocktails.ui.fragments.favorites.mymixes.MyCocktailMixesVM
import com.example.cocktails.ui.fragments.home.HomeFragment
import com.example.cocktails.ui.fragments.home.HomeVM
import com.example.cocktails.ui.fragments.menu.alcoholic.AlcoholicCocktailsCollectionFragment
import com.example.cocktails.ui.fragments.menu.alcoholic.AlcoholicCocktailsVM
import com.example.cocktails.ui.fragments.menu.nonalcoholic.NonAlcoholicCocktailsCollectionFragment
import com.example.cocktails.ui.fragments.menu.nonalcoholic.NonAlcoholicCocktailsVM
import dagger.Binds
import dagger.Module
import dagger.android.ContributesAndroidInjector
import dagger.multibindings.IntoMap

@Module
abstract class ViewModelModule {

    @Binds
    abstract fun bindViewModelFactory(viewModelFactory: ViewModelFactory): ViewModelProvider.Factory

    @ContributesAndroidInjector
    abstract fun contributeHomeFragment(): HomeFragment

    @ContributesAndroidInjector
    abstract fun contributeCocktailDetailsFragment(): CocktailDetailsFragment

    @ContributesAndroidInjector
    abstract fun contributeFavoritesCocktailsFragment(): FavoriteCocktailsFragment

    @ContributesAndroidInjector
    abstract fun contributeMyCocktailMixesFragment(): MyCocktailMixesFragment

    @ContributesAndroidInjector
    abstract fun contributeAlcoholicCocktailsCollectionFragment(): AlcoholicCocktailsCollectionFragment

    @ContributesAndroidInjector
    abstract fun contributeNonAlcoholicCocktailsCollectionFragment(): NonAlcoholicCocktailsCollectionFragment

    @ContributesAndroidInjector
    abstract fun contributeCreateMixDialogFragment(): CreateMixDialogFragment

    @Binds
    @IntoMap
    @ViewModelKey(HomeVM::class)
    abstract fun bindHomeVM(homeVM: HomeVM): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(CocktailDetailsVM::class)
    abstract fun bindCocktailDetailsVM(cocktailDetailsVM: CocktailDetailsVM): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(FavoriteCocktailsVM::class)
    abstract fun bindFavoriteCocktailsVM(favoriteCocktailsVM: FavoriteCocktailsVM): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(MyCocktailMixesVM::class)
    abstract fun bindMyCocktailMixesVM(myCocktailMixesVM: MyCocktailMixesVM): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(AlcoholicCocktailsVM::class)
    abstract fun bindAlcoholicCocktailsVM(alcoholicCocktailsVM: AlcoholicCocktailsVM): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(NonAlcoholicCocktailsVM::class)
    abstract fun bindNonAlcoholicCocktailsVM(nonAlcoholicCocktailsVM: NonAlcoholicCocktailsVM): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(CreateMixDialogViewModel::class)
    abstract fun bindCreateMixVM(createMixVM: CreateMixDialogViewModel): ViewModel
}
