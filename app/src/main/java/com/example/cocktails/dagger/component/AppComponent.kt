package com.example.cocktails.dagger.component

import com.example.cocktails.dagger.modules.AppModule
import com.example.cocktails.dagger.modules.NetworkModule
import com.example.cocktails.dagger.modules.RoomModule
import com.example.cocktails.dagger.modules.ViewModelModule
import dagger.Component
import dagger.android.AndroidInjectionModule
import dagger.android.AndroidInjector
import dagger.android.support.DaggerApplication
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        ViewModelModule::class,
        AndroidInjectionModule::class,
        AppModule::class,
        RoomModule::class,
        NetworkModule::class
    ]
)
interface AppComponent : AndroidInjector<DaggerApplication>