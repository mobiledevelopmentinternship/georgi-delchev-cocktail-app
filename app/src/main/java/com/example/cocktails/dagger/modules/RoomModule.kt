package com.example.cocktails.dagger.modules

import android.app.Application
import androidx.room.Room
import com.example.cocktails.persistance.db.CocktailDb
import dagger.Module
import dagger.Provides

@Module
class RoomModule(application: Application) {

    private val cocktailDatabase = Room.databaseBuilder(application, CocktailDb::class.java, "Cocktails")
        .fallbackToDestructiveMigration()
        .build()

    @Provides
    fun provideDatabase() = cocktailDatabase

    @Provides
    fun provideCocktailDetailsDao() = cocktailDatabase.cocktailDetailsDao()

    @Provides
    fun provideIngredientDao() = cocktailDatabase.ingredientDao()
}