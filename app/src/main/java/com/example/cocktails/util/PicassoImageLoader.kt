package com.example.cocktails.util

import android.text.TextUtils
import android.view.View.GONE
import android.view.View.VISIBLE
import android.widget.ImageView
import com.squareup.picasso.Callback
import com.squareup.picasso.NetworkPolicy
import com.squareup.picasso.Picasso

object PicassoImageLoader {

    fun setImage(imageView: ImageView, imageUrl: String) {
        if (TextUtils.isEmpty(imageUrl)) {
            imageView.visibility = GONE
            return
        }

        Picasso.get()
            .load(imageUrl)
            .networkPolicy(NetworkPolicy.OFFLINE)
            .into(imageView, object : Callback {
                override fun onSuccess() {
                    imageView.visibility = VISIBLE
                }

                override fun onError(e: Exception?) {
                    Picasso.get()
                        .load(imageUrl)
                        .into(imageView, object : Callback {
                            override fun onSuccess() {
                                imageView.visibility = VISIBLE
                            }

                            override fun onError(e: Exception?) {
                                imageView.visibility = GONE
                            }
                        })
                }
            })
    }
}