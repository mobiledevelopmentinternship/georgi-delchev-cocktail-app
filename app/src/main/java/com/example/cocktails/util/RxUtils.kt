package com.example.cocktails.util

import io.reactivex.CompletableTransformer
import io.reactivex.SingleTransformer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.functions.Action
import io.reactivex.internal.operators.completable.CompletableFromAction
import io.reactivex.schedulers.Schedulers

object RxUtils {

    fun <T> applySingleSchedulers(): SingleTransformer<T, T> = SingleTransformer { upstream ->
        upstream
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
    }

    fun applyCompletableSchedulers(): CompletableTransformer = CompletableTransformer { upstream ->
        upstream
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    }

    fun createCompletable(action: Action) = CompletableFromAction(action)
}