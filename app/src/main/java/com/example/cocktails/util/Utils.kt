package com.example.cocktails.util

import com.example.cocktails.persistance.models.IngredientPersist

private val builder = StringBuilder()
private val newLineRegex = Regex("[\\n]")

private fun buildIngredientString(
    ingredient: String,
    measure: String,
    isLast: Boolean = false
) {
    ingredient.replace(newLineRegex, "")
    builder.append(ingredient)
    if (measure.isNotEmpty()) {
        measure.replace(newLineRegex, "")
        builder.append(" - ")
        builder.append(measure)
    }

    if (!isLast) {
        builder.append("\n")
    }
}

fun constructCocktailRecipe(ingredients: List<IngredientPersist>): String {

    ingredients.forEachIndexed { index, ingredientPersist ->
        if (index == ingredients.size - 1) {
            buildIngredientString(
                ingredientPersist.name,
                ingredientPersist.amount,
                true
            )
        } else {
            buildIngredientString(
                ingredientPersist.name,
                ingredientPersist.amount
            )
        }
    }

    val recipe = builder.toString()
    builder.clear()
    return recipe
}